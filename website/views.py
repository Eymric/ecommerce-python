from django.shortcuts import render, redirect
from .models import *
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from .forms import *
from django.contrib.auth.decorators import login_required


def home(request):
    categories = Category.objects.all()
    products = Product.objects.all().order_by('-id')[:3]
    return render(request, 'home.html', {'categories': categories, 'products': products})


def products(request):
    return render(request, 'products.html')


def categories(request):
    categories = Category.objects.all()
    return render(request, 'categories.html', {'categories': categories})


def products_details(request, product_id):
    product = Product.objects.get(id=product_id)
    return render(request, 'products_details.html', {'product': product})


def category_details(request, category_id):
    category = Category.objects.get(id=category_id)
    products = Product.objects.filter(category_id=category.id)
    return render(request, 'category_details.html', {'products': products, 'category': category})


def login_user(request):
    error = False

    if request.method == "POST":
        form = LoginForm(request.POST)
        if form.is_valid():
            username = request.POST["username"]
            password = request.POST["password"]
            print(username, password)
            user = authenticate(username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect('homepage')
            else:  # sinon une erreur sera affichée
                error = True
    else:
        form = LoginForm()

    return render(request, 'login.html', locals())


def logout_user(request):
    logout(request)
    return render(request, 'login.html')


def sign_up(request):
    error = False
    if request.method == "POST":
        form = SignUpForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data["username"]
            email = form.cleaned_data["email"]
            password = form.cleaned_data["password"]
            user = User.objects.create_user(username, email, password)
            if user:
                login(request, user)
            else:
                error = True
    else:
        form = SignUpForm()
    return render(request, 'signup.html', locals())


@login_required
def cart(request):
    current_user = request.user
    cart = Cart.objects.filter(user_id=current_user.id)
    total_price = 0
    for item in cart:
        total_price += item.product.price
    return render(request, 'cart.html', {'cart': cart, 'total_price': total_price})


@login_required
def add_cart(request, product_id):
    if request.user.is_authenticated:
        current_user = request.user
        product = Product.objects.get(id=product_id)
        c = Cart(quantity=1, product_id=product.id, user_id=current_user.id)
        c.save()
        cart = Cart.objects.filter(user_id=current_user.id)
        message = '{0} a été ajouté à votre panier'.format(product.name)
        total_price = 0
        for item in cart:
            total_price += item.product.price
    return render(request, 'cart.html', {'message': message, 'cart': cart, 'total_price': total_price})


@login_required
def delete_cart_item(request, item_id):
    current_user = request.user
    c = Cart.objects.get(id=item_id)
    message = '{0} a bien été supprimé de votre panier'.format(c.product.name)
    c.delete()
    cart = Cart.objects.filter(user_id=current_user.id)
    total_price = 0
    for item in cart:
        total_price += item.product.price
    return render(request, 'cart.html', {'message': message, 'cart': cart, 'total_price': total_price })


def clear_cart(user_id):
    c = Cart.objects.filter(user_id=user_id).delete()


def order(request):
    current_user = request.user
    cart = Cart.objects.filter(user_id=current_user.id)
    if not cart:
        message = 'Votre panier est vide, vous ne pouvez pas commander'
        orders = Order.objects.filter(user_id=current_user.id)
        return render(request, 'orders.html', {'message': message, 'orders': orders})
    else:
        newOrder = Order(user_id=current_user.id)
        newOrder.save()
        orders = Order.objects.filter(user_id=current_user.id)
        for item in cart:
            orderLink = OrderLink(quantity=item.quantity, order_id=newOrder.id, product_id=item.product.id)
            orderLink.save()
        clear_cart(current_user.id)
        message = 'Commande effectuée avec succès !'
        return render(request, 'orders.html', {'message': message, 'orders': orders})


def history_orders(request, order_id):
    details = OrderLink.objects.filter(order_id=order_id)
    order = Order.objects.get(id=order_id)
    total_price = 0
    for item in details:
        total_price += item.product.price
    return render(request, 'history_orders.html', {'details': details, 'order': order, 'total_price': total_price})


def orders(request):
    current_user = request.user
    orders = Order.objects.filter(user_id=current_user.id).order_by('-id')
    return render(request, 'orders.html', {'orders': orders})
