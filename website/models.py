from django.db import models
from django.contrib.auth.models import User


class Category(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return self.name

    def products(self):
        products = Product.objects.filter(category_id=self.id)
        return products


class Product(models.Model):
    name = models.CharField(max_length=40)
    description = models.TextField()
    price = models.FloatField()
    image = models.ImageField()
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.name


class Cart(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    quantity = models.IntegerField()

    def __str__(self):
        return 'Panier de {0}'.format(self.user)

    def total_price(self):
        return self.product.price * self.quantity


class Order(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    date = models.DateTimeField(auto_now=True)

    def __str__(self):
        return 'Commande de {0} fait le {1}'.format(self.user, self.date)

    def products(self):
        ol = OrderLink.objects.filter(order_id=self.id)
        e = list()
        for a in ol:
            e.append(a.product)

        return e

    def total_price(self):
        orders = OrderLink.objects.filter(order_id=self.id)
        total = 0
        for produit in orders:
            print(produit.total_price())
            total += produit.total_price()
        return total


class OrderLink(models.Model):
    order = models.ForeignKey(Order, on_delete=models.DO_NOTHING)
    product = models.ForeignKey(Product, on_delete=models.DO_NOTHING)
    quantity = models.IntegerField()

    def __str__(self):
        return '{0} {1} {2}'.format(self.quantity, self.product, str(self.order))

    def total_price(self):
        return self.product.price * self.quantity
