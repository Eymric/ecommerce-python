from django import forms


class LoginForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)


class SignUpForm(forms.Form):
    username = forms.CharField(label="Nom d'utilisateur", max_length=30)
    email = forms.CharField(label="Email", max_length=60)
    password = forms.CharField(label="Mot de passe", widget=forms.PasswordInput)
