from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include
from . import views
from django.conf import settings


urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.home, name="homepage"),
    path('categories', views.categories, name="categories"),
    path('products/<product_id>', views.products_details, name="products_details"),
    path('categories/<category_id>', views.category_details, name="category_details"),
    path('cart/<product_id>', views.add_cart, name="add_cart"),
    path('login', views.login_user, name="login"),
    path('logout', views.logout_user, name="logout"),
    path('signup', views.sign_up, name="signup"),
    path('cart', views.cart, name="cart"),
    path('cart/delete/<item_id>', views.delete_cart_item, name="delete_cart_item"),
    path('order', views.order, name="order"),
    path('orders', views.orders, name="orders"),
    path('history_orders<order_id>', views.history_orders, name="history_orders"),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
