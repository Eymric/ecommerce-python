from django.contrib import admin
from .models import Product, Category, Order, OrderLink, Cart, User


class ProductAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'price', 'category')
    list_filter = ('name', 'price', 'category')
    ordering = ('category', )
    search_fields = ('name', 'description')


class CartAdmin(admin.ModelAdmin):
    list_display = ('user', 'product', 'quantity', 'total_price')
    list_filter = ('user', )
    ordering = ('user', )
    search_fields = ('user', )

    def total_price(self, obj):
        return obj.total_price()


class OrderLinkAdmin(admin.ModelAdmin):
    list_display = ('order', 'product', 'quantity', 'total_price')
    list_filter = ('order', )
    ordering = ('order', )
    search_fields = ('product', )

    def total_price(self, obj):
        return obj.total_price()


class OrderAdmin(admin.ModelAdmin):
    list_display = ('user', 'date', 'total_price', 'products')
    list_filter = ('user', )
    date_hierarchy = 'date'
    ordering = ('date', )
    search_fields = ('user', )

    def total_price(self, obj):
        return obj.total_price()

    def products(self, obj):
        return obj.products()


admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
admin.site.register(Order, OrderAdmin)
admin.site.register(Cart, CartAdmin)
admin.site.register(OrderLink, OrderLinkAdmin)


